/*

Programa de control para respirador artificial
ESTE CODIGO ES A LOS EFECTOS DE PROTOTIPADO Y NO DE BE SER USADO CON PACIENTES REALES.

TODO:
- do a real initial test function, goto and variable redefinition problems. :(
- Split drawing functions to a diferent file

*/

/*
============ BSD License for U8g2lib Code ============

Universal 8bit Graphics Library (http://code.google.com/p/u8g2/)

Copyright (c) 2016, olikraus@gmail.com
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list 
  of conditions and the following disclaimer.
  
* Redistributions in binary form must reproduce the above copyright notice, this 
  list of conditions and the following disclaimer in the documentation and/or other 
  materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  
 
*/

#include <Arduino.h>

#include <EEPROM.h>

#include "U8g2lib.h"

#define RESOL_V 4.882813
#define ANALOG_REF DEFAULT //INTERNAL1V1

// lcd pins
#define LCD_PINS_RS 16   //[RAMPS14-SMART-ADAPTER]
#define LCD_PINS_MOSI 17 //[RAMPS14-SMART-ADAPTER]
#define LCD_PINS_SCK 23  //[RAMPS14-SMART-ADAPTER]

U8G2_ST7920_128X64_F_SW_SPI u8g2(U8G2_R0, /* clock=*/LCD_PINS_SCK, /* data=*/LCD_PINS_MOSI, /* CS=*/LCD_PINS_RS, /* reset=*/U8X8_PIN_NONE);

#include <Rotary.h>

// encoder
#define ENC_1 33
#define ENC_2 31
#define ENC_BTN 35
int encVal = 0;
bool encBtnState = false;

//red button
#define RED_BUTTON 41
bool run = false;
bool redButtonState = false;

Rotary r = Rotary(ENC_1, ENC_2);

#include "StepperDriverInterrupt.h"

// stepper
#define MOTOR_STEPS 200
#define RPM 100
#define MICROSTEPS 32

#define X_STEP_PIN 54
#define X_DIR_PIN 55
#define X_ENABLE_PIN 38
#define X_MIN_PIN 3
#define X_MAX_PIN 2

#define Y_STEP_PIN 60
#define Y_DIR_PIN 61
#define Y_ENABLE_PIN 56
#define Y_MIN_PIN 14
#define Y_MAX_PIN 15

#define Z_STEP_PIN 46
#define Z_DIR_PIN 48
#define Z_ENABLE_PIN 62
#define Z_MIN_PIN 18
#define Z_MAX_PIN 19

StepperDriverInterrupt stepperInsp(MOTOR_STEPS, X_DIR_PIN, X_STEP_PIN, X_ENABLE_PIN);
StepperDriverInterrupt stepperOxi(MOTOR_STEPS, Y_DIR_PIN, Y_STEP_PIN, Y_ENABLE_PIN);
StepperDriverInterrupt stepperExp(MOTOR_STEPS, Z_DIR_PIN, Z_STEP_PIN, Z_ENABLE_PIN);

#include <PID_v1.h>

//Define Variables we'll be connecting to pid process
double Setpoint, Input, Output;

//Define the inspiration valve and expiration valve tuning Parameters

// -----------------------------  AJUSTES PID

// Umbrales:
#define HI_RPM 30 // si es mas que este valor usa las Kph etc...
// si esta en el medio de usa las Kp etc...
#define LOW_RPM 20 // si es menor que este valor usa las KpL: etc...
double qKp = 1, qKi = 0.08, qKd = 0.1;

double inspKpH = 1.2 * 2.33, inspKiH = 0.35 * 2.33, inspKdH = 0.04 * 2.33;
double expKpH = 0.6 * 2.33, expKiH = 0.2 * 2.33, expKdH = 0.04 * 2.33;

double inspKp = 1.2 * 1.66, inspKi = 0.35 * 1.66, inspKd = 0.04 * 1.66;
double expKp = 0.5 * 1.66, expKi = 0.2 * 1.66, expKd = 0.06 * 1.66;
//-----------------------------------------------------------------------------------------
double inspKpL = 4, inspKiL = 2, inspKdL = 0.1;    //double inspKpL = 3, inspKiL = 0.625, inspKdL = 0.07;
double expKpL = 1.5, expKiL = 0.1, expKdL = 0.03; //double expKpL = 0.55, expKiL = 0.375, expKdL = 0.055;
//-------------------------------------------

//Specify the links and initial tuning parameters
PID presPID(&Input, &Output, &Setpoint, inspKp, inspKi, inspKd, P_ON_E, DIRECT);
int SampleTime = 20;

//Electrovalves output pins
#define INSP_AIR_VALVE_PIN 10
#define INSP_OXI_VALVE_PIN 9

//Buzzer pin. (must have internal oscilator).
#define BUZZ_PIN 37
bool sound = false;

//Presure sensor.
#define PRESURE_PIN A9
#define SPEED_PIN A5

#include <SimpleKalmanFilter.h>
/*
 SimpleKalmanFilter(e_mea, e_est, q);
 e_mea: Measurement Uncertainty 
 e_est: Estimation Uncertainty 
 q: Process Noise
*/
SimpleKalmanFilter presureFilter(10, 2, 0.3);
SimpleKalmanFilter speedPresureFilter(10, 2, 0.3);

// Define settings parameters

struct parameter
{
  byte id;
  String longName;
  String name;
  String unit;
  float value;
  bool showDecimals;
  float min;
  float max;
};

parameter pSet = {0, "Presion: ", "Pmax:", "cmH2O", 20, false, 5, 60};
parameter pAlMax = {1, "Alarma max: ", "Al Max:", "cmH2O", 50, false, 10, 75};
parameter pAlMin = {2, "Alarma min: ", "Al Min:", "cmH2O", 2, false, 0, 5};
parameter freq = {3, "Frecuencia: ", "Frec:", "rpm", 15, false, 5, 60};
parameter pInsp = {4, "Pausa de insp: ", "P insp.:", "s", 0, true, 0, 3};
parameter peep = {5, "PEEP: ", "PEEP:", "cmH2O", 10, false, 0, 20};
// parameter prop = {6, "Tiempo de insp/exp: 1/", "I:E: 1/", "", 2, false, 1, 4};
parameter oxig = {7, "Oxigeno: ", "Oxigeno:", "%", 0, false, 0, 16};
parameter volSet = {8, "Volumen: ", "Vol:", "ml", 400, false, 25, 1000};
parameter volAlMax = {9, "Alarma max: ", "Al Max:", "ml", 1100, false, 50, 1100};
parameter volAlMin = {10, "Alarma min: ", "Al Min:", "ml", 500, false, 0, 500};
parameter curve = {11, "Curva: ", "Crv:", "", 0, false, 0, 1};
parameter tInsp = {12, "Tiempo de insp.: ", "T.insp:", "", 1.5, true, 0, 6};
parameter graphMode = {13, "Grafico: ", "", "", 0, false, 0, 1};
parameter back = {14, "VOLVER", "", "", 0, false, 0, 1}; // dumy for back button

#define N_PARAMS 9
parameter *paramsMenu[N_PARAMS] = {&freq, &pSet, &volSet, &tInsp, &peep, &pInsp, &oxig, &graphMode, &back};
parameter *subParam[4] = {&pAlMax, &pAlMin, &volAlMax, &volAlMin};

byte oxSteps[17] = {  21, 25, 30, 35,  40,  45,  50,  55,  60,  65,  70,  75,  80,  85,  90, 95, 100};
float oxCoef[17] = { 0.9,4.8,2.4,1.7,1.38,1.14,1.03,0.94,0.87,0.82,0.79,0.75,0.73,0.71,0.71,0.72, 1.0};
// (30 - 2.5) - (35 - 1.75)
struct storeSettings
{
  char stored;
  float freq;
  float pSet;
  float volSet;
  float tInsp;
  float peep;
  float pInsp;
  float oxig;
  float grapMode;
  float pAlMax;
  float pAlMin;
  float volAlMax;
  float volAlMin;
};

// Define process variables

struct value
{
  byte id;
  String longName;
  String name;
  String unit;
  float value;
  bool showDecimals;
};

value volume = {100, "Volumen: ", "VTi:", "ml", 0, false};
value presure = {101, "Presion: ", "PVAm:", "cmH2O", 0, false};
value pip = {102, "P. max pico: ", "PIP:", "cmH2O", 0, false};
value ie = {103, "I:E ", "I:E: 1:", "", 0, true};
value q = {104, "Qi ", "Qi ", "", 0, true};
value realPeep = {104, "PEEP ", "PEEP ", "cmH2O", 0, true};

#define N_INFO 6
void *infoItems[N_INFO] = {&pip, &realPeep, &freq, &volume, &ie, &oxig};

float volumeIntegration = 0;

// initial test vars
#define TEST_INTERVAL 0.5 // tiempo para duracion del testeo de presion
#define DELTA_P -20       // diferencia de presion admisible
#define SYSTEM_C 5        // compliance admisible
#define TEST_PRESURE 40   //setpoint para pruebas
#define C_CALC_PRESURE 30
#define TEST_TINSP 1
float systemC = 0;
bool tested = false;

// Menu control and drawing vars
byte setIndexMenu = 0;
byte setIndexSetting = 0;
byte setSubParamIndex = 0;
byte lastIndexMenu = 0;
byte lastIndexSetting = 0;
byte maxIndex = 0;
byte curretnPage = 0;

bool setInfoMenu = false;
bool setNavMenu = false;
bool setNavSetting = false;
bool buttonChange = false;
bool indexChange = false;
bool settingChange = true;
bool firstShow = false;
bool pauseShowed = false;

#define GRAPH_WIDTH 128
#define GRAPH_HEIGTH 32
#define GRAPH_Y_OFFSET 8
#define GRAPH_SCALE_TEXT_OFFSET 12
byte graphBuffDrawCursor = GRAPH_SCALE_TEXT_OFFSET;
bool graphUpdate = false;
bool infoUpdate = false;
byte infoItem = 0;

byte yBox = 0;

//alarms and notif
#define NO_ALARM 0
bool fullScreenAlarm = false;
#define CLEAR_NTIF 1
bool pendingNotif = false;
#define E_STOP 2
bool emergencyStop = false;
#define P_NTIF 3
bool presureNotif = false;
#define P_MAX 4
bool presureMaxAlarm = false;
#define P_MIN 5
bool presureMinAlarm = false;
#define V_MAX 6
bool volumeMaxAlarm = false;
#define V_MIN 7
bool volumeMinAlarm = false;

// byte currentAlarm = NO_ALARM;
bool alarm = false;
byte beepMillis = 0;
int alarmTimer = 0;
#define ALARM_TIME 10

//valve conrol vars
#define INSP_VALVE_MOTOR_STEPS_FROM_HOME 950 // 975
#define OXI_VALVE_MOTOR_STEPS_FROM_HOME 910  // 915
#define EXP_VALVE_MOTOR_STEPS_FROM_HOME 730  //
//MODALIDAD MAXI:
#define INSP_VALVE_MOTOR_MAX_TRAVEL 500  //500 microsteps full open 725
#define INSP_VALVE_MOTOR_MIN_TRAVEL 225  //225 microsteps min open 200
#define OXI_VALVE_MOTOR_MAX_TRAVEL  500  //500 microsteps full open 725
#define OXI_VALVE_MOTOR_MIN_TRAVEL  225  //225 microsteps min open 200
#define EXP_VALVE_MOTOR_MAX_TRAVEL  7000 //6000 microsteps full open 725
#define EXP_VALVE_MOTOR_MIN_TRAVEL  200  //500 microsteps min open 200

#define INSP_VALVE_EXP 0.7
#define OXI_VALVE_EXP 0.7  //0.7
#define EXP_VALVE_EXP 0.7

#define VALVE_TO_LINEAL_EXPONET 0.65

#define VALVE_CLOSE_TIME 0.1

#define HOLD_PROP 0.3 // part of insp tiime on hold.

//MODALIDAD DANI:
//rango 0 - 300
#define INSP_VALVE_MOTOR_MAX_TRAVEL_A 500  //500 microsteps full open 725
#define OXI_VALVE_MOTOR_MAX_TRAVEL_A 500  //500 microsteps full open 725
#define EXP_VALVE_MOTOR_MAX_TRAVEL_A 6000 //6000 microsteps full open 725
#define INSP_VALVE_EXP_A 0.7
#define OXI_VALVE_EXP_A 0.7
#define EXP_VALVE_EXP_A 0.7

// rango 300 - 600
#define INSP_VALVE_MOTOR_MAX_TRAVEL_B 500  //500 microsteps full open 725
#define OXI_VALVE_MOTOR_MAX_TRAVEL_B 500  //500 microsteps full open 725
#define EXP_VALVE_MOTOR_MAX_TRAVEL_B 6000 //6000 microsteps full open 725
#define INSP_VALVE_EXP_B 0.7
#define OXI_VALVE_EXP_B 0.7
#define EXP_VALVE_EXP_B 0.7

//rango 600 - 1000
#define INSP_VALVE_MOTOR_MAX_TRAVEL_C 500  //500 microsteps full open 725
#define OXI_VALVE_MOTOR_MAX_TRAVEL_C 500  //500 microsteps full open 725
#define EXP_VALVE_MOTOR_MAX_TRAVEL_C 6000 //6000 microsteps full open 725
#define INSP_VALVE_EXP_C 0.7
#define OXI_VALVE_EXP_C 0.7
#define EXP_VALVE_EXP_C 0.7

const float VOL_STEP_H = (1.0 / 1000.0); //multiplica a el max travel de las valulas y a el vol seteado

//la velocidad de la valvula es proporcional segun la distancia a recorrer.
//la maxima velocidad se logra cuando quiere hacer un salto de todo el recorrido (MAX_TRAVEL)
// con la minima determinas la minima. osea que por mas chico que sea el movimiento no puede ir mas lento que eso.
#define MOTORS_MAX_SPEED 370
#define MOTORS_MIN_SPEED 1

#define INSP_VALVE 1
#define EXP_VALVE 2
float inspValveState = 100;
float expValveState = 0;
float inspTime = 0;
float expTime = 0;
float cycleTime = 0;
bool inspPrevDir = false;
// int cmpVal = 0;
float qInsp = 0;

//cycle vars
#define MODE true // false q controled, true presure controled.

//this must be enum :S
#define STOP 0
#define START 1
#define INSP_OPEN 2
#define INSP_HOLD 7
#define INSP_CLOSE 3
#define EXP_OPEN 4
#define EXP_CLOSE 5
#define ALARM_INSP_CLOSE 6

byte cycleCount = 0;
byte cycleState = STOP;

float cycleElapsedTime = 0;
// int remainSteps = 0;
bool recalcCycle = true;
float remanentPres = 0;

// ISR vars
unsigned long millisCounter = 0;
unsigned long millisCounterGraph = 0;

//timer measure vars
unsigned long endTime = 0;
unsigned long startTime = 0;

// ---------------------------------------  Eeprom functions
#define EPPROM_V 'E'

char recallStoredState()
{
  storeSettings recallThis;
  EEPROM.get(0, recallThis);
  return recallThis.stored;
}

void recallAll()
{

  storeSettings recallThis;
  EEPROM.get(0, recallThis);

  freq.value = recallThis.freq;
  pSet.value = recallThis.pSet;
  volSet.value = recallThis.volSet;
  tInsp.value = recallThis.tInsp;
  peep.value = recallThis.peep;
  pInsp.value = recallThis.pInsp;
  oxig.value = recallThis.oxig;
  graphMode.value = recallThis.grapMode;
  pAlMax.value = recallThis.pAlMax;
  pAlMin.value = recallThis.pAlMin;
  volAlMax.value = recallThis.volAlMax;
  volAlMin.value = recallThis.volAlMin;
}

void storeAll()
{
  storeSettings storeThis = {
      EPPROM_V,
      freq.value,
      pSet.value,
      volSet.value,
      tInsp.value,
      peep.value,
      pInsp.value,
      oxig.value,
      graphMode.value,
      pAlMax.value,
      pAlMin.value,
      volAlMax.value,
      volAlMin.value};

  EEPROM.put(0, storeThis);
}

// ---------------------------------------  Settings and menu functions

void drawSetting(byte x_offset, byte col, byte row, parameter *param, bool selected, bool longName = false, bool edit = false)
{
  byte xPos = (64 * col) + x_offset;
  byte yPos = (row * 8);
  u8g2.setDrawColor(0);
  longName ? u8g2.drawBox(xPos, yPos + 1, 123 - x_offset, 8) : u8g2.drawBox(xPos, yPos + 1, 63, 8);
  selected && !edit ? u8g2.setDrawColor(0) : u8g2.setDrawColor(1);
  u8g2.setCursor(xPos, 8 + yPos);
  u8g2.print(longName ? param->longName : param->name);
  selected &&edit ? u8g2.setDrawColor(0) : u8g2.setDrawColor(1);

  if (param->id == 5 && param->value == 0)
  {
    u8g2.print("off");
  }
  else if (param->id == oxig.id)
  {
    u8g2.println(param->unit);
    if (longName)
      u8g2.print(" ");
    u8g2.print(oxSteps[(int)param->value]);
  }
  else if (param->id == graphMode.id)
  {
    if (param->value)
    {
      if (!longName)
        selected && !edit ? u8g2.setDrawColor(0) : u8g2.setDrawColor(1);
      u8g2.print("Vol/Tiempo");
    }
    else
    {
      if (!longName)
        selected && !edit ? u8g2.setDrawColor(0) : u8g2.setDrawColor(1);
      u8g2.print("Pres/Tiempo");
    }
  }
  else
  {
    if (param->id != back.id)
    {
      if (param->showDecimals)
      {
        u8g2.print(param->value);
      }
      else
      {
        u8g2.print((int)param->value);
      }
      u8g2.print(param->unit);
    }
  }
}

void drawFastValue(byte x_offset, byte col, byte row, parameter *param)
{
  byte xPos = (64 * col) + x_offset;
  byte yPos = (row * 8);

  u8g2.setDrawColor(1);
  u8g2.setCursor(xPos, 8 + yPos);

  u8g2.print(param->name);

  if (param->showDecimals)
  {
    u8g2.print(param->value);
  }
  else
  {
    u8g2.print((int)param->value);
  }

  u8g2.print(param->unit);

  if (param->value < 1000)
    u8g2.print(" ");

  if (param->value < 100)
    u8g2.print(" ");

  if (param->value < 10)
    u8g2.print(" ");
}

void drawPauseMesg()
{
  pauseShowed = true;
  u8g2.setDrawColor(0);
  u8g2.drawBox(0, 0, 128, 41);

  u8g2.setDrawColor(1);
  u8g2.setFont(u8g2_font_8x13B_mf);
  u8g2.setCursor(5, 20);
  u8g2.println("Unidad Pausada!");
  u8g2.setFont(u8g2_font_5x7_mf);
}

void drawAccept(byte x_offset, byte col, byte row, bool selected)
{
  byte xPos = (64 * col) + x_offset;
  byte yPos = (row * 8);
  selected ? u8g2.setDrawColor(0) : u8g2.setDrawColor(1);
  u8g2.setCursor(xPos, 8 + yPos);
  u8g2.print("ACEPTAR");
}

void infoFullDraw()
{
  if (run)
  {
    //graph frame.
    u8g2.setDrawColor(1);
    u8g2.drawFrame(0, 0, GRAPH_WIDTH, GRAPH_Y_OFFSET + GRAPH_HEIGTH + 1);

    //graph scale
    int fullScale = 0;
    if (graphMode.value == 0)
    {
      fullScale = pSet.value * 1.25;
    }
    else
    {
      fullScale = volSet.value * 1.25;
    }

    u8g2.setFont(u8g2_font_4x6_mf);
    for (int i = 0; i < 4; i++)
    {
      int s = (int)(fullScale * (1 - ((float)i / (float)3)));
      u8g2.setCursor(s < 10 ? 4 : 2, 7 * i + 16);
      u8g2.print(s);
    }
    u8g2.setFont(u8g2_font_5x7_mf);

    //graph format
    drawSetting(0, 1, 0, &graphMode, false);
  }
  else
  {
    drawPauseMesg();
  }

  // settings and realtime values
  for (byte i = 0; i < 2; i++) //col
  {
    for (byte j = 0; j < 3; j++) //row
    {
      byte p = (i * 3) + j;
      if (p < N_PARAMS)
        drawSetting(0, i, j + 5, infoItems[p], false);
    }
  }
}

void showHome()
{
  u8g2.setFont(u8g2_font_5x7_mf);
  u8g2.clearBuffer();
  infoFullDraw();
  u8g2.sendBuffer();
  setIndexMenu = 0;
  setInfoMenu = false;
  setNavMenu = false;
  setNavSetting = false;
}

void menuFullDraw(void)
{
  //draw params
  for (byte j = 0; j < 8; j++) //row
  {
    byte p = j + (setIndexMenu > 7 ? 8 : 0);
    if (setIndexMenu == p && p < N_PARAMS)
    {
      drawSetting(5, 0, j, paramsMenu[p], true, true);
    }
    else if (p < N_PARAMS)
    {
      drawSetting(5, 0, j, paramsMenu[p], false, true);
    }
  }

  //drae scroll bar
  u8g2.drawFrame(124, 0, 4, 64);
  u8g2.drawBox(124, curretnPage * 32, 4, 32);
}

void drawSttingsMenu(void)
{
  if ((buttonChange || indexChange) && !pendingNotif)
  {
    if (setIndexMenu / 8 != curretnPage)
    {
      curretnPage = setIndexMenu / 8;
      u8g2.clearBuffer();
      menuFullDraw();
    }

    // u8g2.clearBuffer();
    for (byte j = 0; j < 8; j++) //row
    {
      byte p = j + (setIndexMenu > 7 ? 8 : 0);
      if (setIndexMenu == p && p < N_PARAMS)
      {
        drawSetting(5, 0, j, paramsMenu[p], true, true);
      }
      // else if (p < N_PARAMS)
      if (lastIndexMenu == p && indexChange && p < N_PARAMS)
      {
        drawSetting(5, 0, j, paramsMenu[p], false, true);
      }
    }

    if (indexChange && !setNavMenu)
      lastIndexMenu = setIndexMenu;

    buttonChange = false;
    indexChange = false;
  }
}

void showSettingsMenu()
{
  u8g2.setFont(u8g2_font_5x7_mf);
  u8g2.clearBuffer();
  menuFullDraw();
  u8g2.sendBuffer();
  // setIndexMenu = 0;
  setNavMenu = false;
  setInfoMenu = true;
  buttonChange = true;
  drawSttingsMenu();
}

void drawSettingPage()
{

  if (buttonChange || indexChange || settingChange)
  {
    if (!setNavSetting && buttonChange) //auto next.
    {
      setIndexSetting++;
      indexChange = true;
    }
    setSubParamIndex = 255;

    if (setIndexSetting > 200) // to avoid use ints chech for byte -1 as 255
      setIndexSetting = maxIndex;
    if (setIndexSetting > maxIndex)
      setIndexSetting = 0;

    maxIndex = 1;

    if (setIndexSetting == 0)
    {
      drawSetting(10, 0, 2, paramsMenu[setIndexMenu], true, true, setNavSetting);
    }
    if (lastIndexSetting == 0 && indexChange)
    {
      drawSetting(10, 0, 2, paramsMenu[setIndexMenu], false, true, false);
    }

    if (paramsMenu[setIndexMenu]->id == pSet.id)
    {
      // if setting is pres draw pres max
      maxIndex += 2;
      if (setIndexSetting == 1)
      {
        drawSetting(10, 0, 3, &pAlMax, true, true, setNavSetting);
        setSubParamIndex = 0;
      }
      if (firstShow || (lastIndexSetting == 1 && indexChange))
      {
        drawSetting(10, 0, 3, &pAlMax, false, true, false);
      }
      if (setIndexSetting == 2)
      {
        drawSetting(10, 0, 4, &pAlMin, true, true, setNavSetting);
        setSubParamIndex = 1;
      }
      if (firstShow || (lastIndexSetting == 2 && indexChange))
      {
        drawSetting(10, 0, 4, &pAlMin, false, true, false);
      }
    }

    if (paramsMenu[setIndexMenu]->id == volSet.id)
    {
      // if setting is pres draw pres max
      maxIndex += 2;
      if (setIndexSetting == 1)
      {
        drawSetting(10, 0, 3, &volAlMax, true, true, setNavSetting);
        setSubParamIndex = 2;
      }
      if (firstShow || (lastIndexSetting == 1 && indexChange))
      {
        drawSetting(10, 0, 3, &volAlMax, false, true, false);
      }
      if (setIndexSetting == 2)
      {
        drawSetting(10, 0, 4, &volAlMin, true, true, setNavSetting);
        setSubParamIndex = 3;
      }
      if (firstShow || (lastIndexSetting == 2 && indexChange))
      {
        drawSetting(10, 0, 4, &volAlMin, false, true, false);
      }
    }

    if (setIndexSetting == maxIndex)
    {
      drawAccept(8, 0, 7, true);
      if (setNavSetting)
      {
        setNavSetting = false;
        showSettingsMenu();
      }
    }
    else
    {
      drawAccept(8, 0, 7, false);
    }

    lastIndexSetting = setIndexSetting;
    buttonChange = false;
    indexChange = false;
    settingChange = false;
    if (firstShow)
      firstShow = false;
  }
}

void drawGraph()
{
  int graphValue = 0;
  // int sp = (Setpoint / (float)pSet.value) * (GRAPH_HEIGTH - 2);

  if (graphMode.value == 0)
  {
    graphValue = (presure.value / (pSet.value * 1.25)) * (GRAPH_HEIGTH - 2);
  }
  else
  {
    graphValue = (volume.value / (volSet.value * 1.25)) * (GRAPH_HEIGTH - 2);
  }

  if (graphValue < 0)
    graphValue = 0;
  if (graphValue > GRAPH_HEIGTH - 2)
    graphValue = GRAPH_HEIGTH - 2;

  //14404 full travel steps at 1000cc
  u8g2.setDrawColor(0);
  u8g2.drawVLine(graphBuffDrawCursor, GRAPH_Y_OFFSET + 1, GRAPH_HEIGTH - 1);
  u8g2.setDrawColor(1);

  u8g2.drawPixel(graphBuffDrawCursor, GRAPH_Y_OFFSET + GRAPH_HEIGTH - graphValue - 1);
  // u8g2.drawPixel(graphBuffDrawCursor, GRAPH_Y_OFFSET + GRAPH_HEIGTH - graphValue - 1);

  graphBuffDrawCursor++;
  if (graphBuffDrawCursor > GRAPH_WIDTH - 2)
    graphBuffDrawCursor = GRAPH_SCALE_TEXT_OFFSET;
}

void drawInfo()
{
  // u8g2.setDrawColor(1);

  // u8g2.setCursor(64, 8 * 6);
  // u8g2.print("Vol:");
  // u8g2.print((int)volume.value);
  // u8g2.print("cc");
  // u8g2.print(" ");

  // u8g2.setCursor(0, 8 * 8);
  // u8g2.print("PIP:");
  // u8g2.print((int)pip.value);
  // u8g2.print(pSet.unit);
  // u8g2.print(" ");

  // u8g2.setCursor(0, 8 * 7);
  // u8g2.print("PVAm:");
  // u8g2.setCursor(25, 8 * 7);
  // u8g2.print((int)presure.value);
  // u8g2.print(pSet.unit);
  // u8g2.print(" ");

  // real time values
  if (infoUpdate)
  {
    infoUpdate = false;
    if (infoItem > 2)
      infoItem = 0;
    switch (infoItem)
    {
    case 0:
      drawFastValue(0, 0, 5, infoItems[0]);
      break;
    case 1:
      drawFastValue(0, 1, 5, infoItems[3]);
      break;
    case 2:
      drawFastValue(0, 0, 6, infoItems[1]);
      break;
    }
    infoItem++;
  }
}

void drawNotif(byte type, bool fullScreen = false)
{
  if (type == CLEAR_NTIF)
  {
    u8g2.setDrawColor(0);
    u8g2.drawBox(0, (4 * 8) + 1, 128, 8);
  }
  else
  {
    pendingNotif = true;
    if (fullScreen)
    {
      // pendingNotif = true;
      fullScreenAlarm = true;
      u8g2.clearBuffer();
      u8g2.setDrawColor(1);
      u8g2.setCursor(20, (2 * 8) + 8);
      u8g2.print(" ALARMA! ");
      u8g2.setCursor(15, (3 * 8) + 8);
      switch (type)
      {
      case E_STOP:
        u8g2.setCursor(20, (3 * 8) + 8);
        u8g2.print(" PARADA DE EMERGENCIA");
        break;
      case P_MAX:
        u8g2.print(" PRESION MAXIMA ");
        break;
      case P_MIN:
        u8g2.print(" PRESION MINIMA ");
        break;
      case P_NTIF:
        u8g2.print(" PRESION SETEADA ");
        break;
      case V_MAX:
        u8g2.print(" VOLUMEN MAXIMO ");
        break;
      case V_MIN:
        u8g2.print(" VOLUMEN MINIMO ");
        break;
      }
      u8g2.setCursor(15, (7 * 8) + 8);
      u8g2.setDrawColor(0);
      u8g2.print("ACEPTAR");
      u8g2.sendBuffer();
    }
    else
    {
      u8g2.setDrawColor(0);
      u8g2.setCursor(0, (4 * 8) + 8);
      switch (type)
      {
      case P_MAX:
      case P_MIN:
        u8g2.print(" ALARMA: ");
      case P_NTIF:
        u8g2.print(" Presion ! ");
        break;
      case V_MAX:
        u8g2.print(" ALARMA: ");
        u8g2.print(" Volumen max ! ");
        break;
      }
    }
  }
}

void drawMessage(String s)
{
  char *text = s.c_str();
  u8g2.setFont(u8g2_font_5x7_mf);
  u8g2.clearBuffer();
  int line = 10;

  // Returns first token
  char *token = strtok(text, "|");

  // Keep printing tokens while one of the
  // delimiters present in str[].
  while (token != NULL)
  {
    u8g2.setCursor(0, line);
    u8g2.print(token);
    line += 10;
    token = strtok(NULL, "|");
  }
  u8g2.sendBuffer();
}

void showSettingPage()
{
  u8g2.setFont(u8g2_font_5x7_mf);
  u8g2.clearBuffer();
  setIndexSetting = 0;
  lastIndexSetting = -1;
  setNavMenu = true;
  setNavSetting = true;
  buttonChange = true;
  firstShow = true;
  pauseShowed = false;
}

void updateScreen()
{
  //update screen.
  u8g2.updateDisplayArea(0, yBox, 16, 1);
  yBox++;
  if (yBox > 7)
    (yBox = 0);
}

void editSetting(byte index, int deltaVal, bool isSubParam = false)
{
  if (!isSubParam)
  {
    if (paramsMenu[index]->id == tInsp.id || paramsMenu[index]->id == freq.id || paramsMenu[index]->id == pSet.id || paramsMenu[index]->id == graphMode.id)
      recalcCycle = true;

    if (paramsMenu[index]->id == tInsp.id)
    {
      paramsMenu[index]->value += deltaVal * 0.01;
    }
    else if (paramsMenu[index]->id == pInsp.id)
    {
      paramsMenu[index]->value += deltaVal * 0.1;
    }
    else if (paramsMenu[index]->id == volSet.id)
    {
      paramsMenu[index]->value += deltaVal * 25;
    }
    else
    {
      paramsMenu[index]->value += deltaVal;
    }

    if (paramsMenu[index]->value > paramsMenu[index]->max)
      paramsMenu[index]->value = paramsMenu[index]->max;
    if (paramsMenu[index]->value < paramsMenu[index]->min)
      paramsMenu[index]->value = paramsMenu[index]->min;
  }
  else
  {
    //subparams
    if (subParam[index]->id == volAlMax.id || subParam[index]->id == volAlMin.id)
    {
      subParam[index]->value += deltaVal * 25;
    }
    else
    {
      subParam[index]->value += deltaVal;
    }
    if (subParam[index]->value > subParam[index]->max)
      subParam[index]->value = subParam[index]->max;
    if (subParam[index]->value < subParam[index]->min)
      subParam[index]->value = subParam[index]->min;
  }
}

void doSetting()
{
  if (setInfoMenu)
  {
    if (!setNavMenu)
    {
      //on menu screen
      setIndexMenu += encVal;
      if (encVal != 0)
        indexChange = true;
      if (setIndexMenu > 200) // to avoid use ints chech for byte -1 as 255
        setIndexMenu = N_PARAMS - 1;
      if (setIndexMenu > N_PARAMS - 1)
        setIndexMenu = 0;
    }
    else
    {
      //on setting screen
      if (!setNavSetting)
      {
        setIndexSetting += encVal;
        if (encVal != 0)
          indexChange = true;
      }
      else
      {
        if (encVal != 0)
          settingChange = true;

        if (setSubParamIndex == 255)
        {
          editSetting(setIndexMenu, encVal);
        }
        else
        {
          editSetting(setSubParamIndex, encVal, true);
        }
      }
    }
    encVal = 0;
  }
}

// ----------------------------------------  VALVE CONTROL ---

void setCycle()
{
  cycleTime = (float)60 / (float)freq.value;
  // inspTime = cycleTime / (prop.value + 1);
  // expTime = cycleTime - (cycleTime / (prop.value + 1));
  // presPID.SetOutputLimits(-1 * pSet.value, pSet.value);

  tInsp.max = cycleTime / 2;

  if (tInsp.value > tInsp.max)
  {
    tInsp.value = tInsp.max;
  }

  inspTime = tInsp.value;
  expTime = cycleTime - tInsp.value;

  if (graphMode.value == 0)
  {
    presPID.SetOutputLimits(-1 * pSet.value, pSet.value);
  }
  else
  {
    presPID.SetOutputLimits(-1 * volSet.value, volSet.value);
  }
  ie.value = expTime / inspTime;

  // Serial.print(cycleTime);
  // Serial.print("|");
  // Serial.print(inspTime);
  // Serial.print("|");
  // Serial.println(expTime);
}

float floatMap(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

float getOxiProp()
{
  return (float)(oxSteps[(int)oxig.value] - 21)* oxCoef[(int)oxig.value] * ((float)1 / (float)79);
}

float getAirProp()
{
  return 1 - getOxiProp();
}

void setValveState(byte valve, float openP, float time = 0)
{
  openP = constrain(openP, 0, 100);
  // openP = 100 * pow((float)openP / (float)100, 0.45);

  int microstepToMove = 0;
  if (valve == INSP_VALVE)
  {
    
    int inspValveCompMaxTravel = 0;

    if (!tested)
    {
      inspValveCompMaxTravel = (int)((float)INSP_VALVE_MOTOR_MAX_TRAVEL * pow(1, INSP_VALVE_EXP));
      inspValveCompMaxTravel += INSP_VALVE_MOTOR_MIN_TRAVEL;
    }
    else
    {
      inspValveCompMaxTravel = (int)((float)INSP_VALVE_MOTOR_MAX_TRAVEL * pow((volSet.value * VOL_STEP_H), INSP_VALVE_EXP));
      inspValveCompMaxTravel = (int)(float)inspValveCompMaxTravel / (float)((tInsp.value*0.25)+0.75);
      inspValveCompMaxTravel += INSP_VALVE_MOTOR_MIN_TRAVEL;

      if (inspValveCompMaxTravel > (INSP_VALVE_MOTOR_MAX_TRAVEL * pow(1, INSP_VALVE_EXP)) + INSP_VALVE_MOTOR_MIN_TRAVEL)
      {
        inspValveCompMaxTravel = (int)((float)INSP_VALVE_MOTOR_MAX_TRAVEL * pow(1, INSP_VALVE_EXP));
        inspValveCompMaxTravel += INSP_VALVE_MOTOR_MIN_TRAVEL;
      }

      /*/rangos
      if (volume.value < 300)
      {
        inspValveCompMaxTravel = (int)((float)INSP_VALVE_MOTOR_MAX_TRAVEL_A * pow((volSet.value * VOL_STEP_H), INSP_VALVE_EXP_A));
        inspValveCompMaxTravel = (int)(float)inspValveCompMaxTravel / (float)((tInsp.value*0.5)+0.5);
        inspValveCompMaxTravel += INSP_VALVE_MOTOR_MIN_TRAVEL;
        if (inspValveCompMaxTravel > (INSP_VALVE_MOTOR_MAX_TRAVEL_A * pow(1, INSP_VALVE_EXP_A)) + INSP_VALVE_MOTOR_MIN_TRAVEL)
        {
          inspValveCompMaxTravel = (int)((float)INSP_VALVE_MOTOR_MAX_TRAVEL_A * pow(1, INSP_VALVE_EXP_A));
          inspValveCompMaxTravel += INSP_VALVE_MOTOR_MIN_TRAVEL;
        }
      }
      else if (volume.value > 300 && volume.value < 600)
      {
        inspValveCompMaxTravel = (int)((float)INSP_VALVE_MOTOR_MAX_TRAVEL_B * pow((volSet.value * VOL_STEP_H), INSP_VALVE_EXP_B));
        inspValveCompMaxTravel = (int)(float)inspValveCompMaxTravel / (float)((tInsp.value*0.5)+0.5);
        inspValveCompMaxTravel += INSP_VALVE_MOTOR_MIN_TRAVEL;
        if (inspValveCompMaxTravel > (INSP_VALVE_MOTOR_MAX_TRAVEL_B * pow(1, INSP_VALVE_EXP_B)) + INSP_VALVE_MOTOR_MIN_TRAVEL)
        {
          inspValveCompMaxTravel = (int)((float)INSP_VALVE_MOTOR_MAX_TRAVEL_B * pow(1, INSP_VALVE_EXP_B));
          inspValveCompMaxTravel += INSP_VALVE_MOTOR_MIN_TRAVEL;
        }
      }
      else if (volume.value > 600)
      {
        inspValveCompMaxTravel = (int)((float)INSP_VALVE_MOTOR_MAX_TRAVEL_C * pow((volSet.value * VOL_STEP_H), INSP_VALVE_EXP_C));
        inspValveCompMaxTravel = (int)(float)inspValveCompMaxTravel / (float)((tInsp.value*0.5)+0.5);
        inspValveCompMaxTravel += INSP_VALVE_MOTOR_MIN_TRAVEL;
        if (inspValveCompMaxTravel > (INSP_VALVE_MOTOR_MAX_TRAVEL_C * pow(1, INSP_VALVE_EXP_C)) + INSP_VALVE_MOTOR_MIN_TRAVEL)
        {
          inspValveCompMaxTravel = (int)((float)INSP_VALVE_MOTOR_MAX_TRAVEL_C * pow(1, INSP_VALVE_EXP_C));
          inspValveCompMaxTravel += INSP_VALVE_MOTOR_MIN_TRAVEL;
        }
      }
      //hasta aca*/
    }

    float openPair = 100 * pow(((float)openP * getAirProp()) / (float)100, VALVE_TO_LINEAL_EXPONET);

    // airvalve
    microstepToMove = (int)(((float)inspValveCompMaxTravel / 100.0) * openPair) - stepperInsp.position;
    stepperInsp.setRPM(constrain(map(abs(microstepToMove), 0, inspValveCompMaxTravel, MOTORS_MIN_SPEED, MOTORS_MAX_SPEED), MOTORS_MIN_SPEED, MOTORS_MAX_SPEED));
    stepperInsp.startMove(microstepToMove);

    // int oxiValvCompMaxTravel = OXI_VALVE_MOTOR_MAX_TRAVEL + OXI_VALVE_MOTOR_MIN_TRAVEL;
    int oxiValveCompMaxTravel = 0;

    if (!tested)
    {
      oxiValveCompMaxTravel = (int)((float)OXI_VALVE_MOTOR_MAX_TRAVEL * pow(1, OXI_VALVE_EXP));
      oxiValveCompMaxTravel += OXI_VALVE_MOTOR_MIN_TRAVEL;
    }
    else
    {
      oxiValveCompMaxTravel = (int)((float)OXI_VALVE_MOTOR_MAX_TRAVEL * pow((volSet.value * VOL_STEP_H), OXI_VALVE_EXP));
      oxiValveCompMaxTravel = (int)(float)oxiValveCompMaxTravel / (float)((tInsp.value*0.25)+0.75);
      oxiValveCompMaxTravel += OXI_VALVE_MOTOR_MIN_TRAVEL;
      
      if (oxiValveCompMaxTravel > (OXI_VALVE_MOTOR_MAX_TRAVEL* pow(1, OXI_VALVE_EXP)) + OXI_VALVE_MOTOR_MIN_TRAVEL)
      {
        oxiValveCompMaxTravel = (int)((float)OXI_VALVE_MOTOR_MAX_TRAVEL * pow(1, OXI_VALVE_EXP));
        oxiValveCompMaxTravel += OXI_VALVE_MOTOR_MIN_TRAVEL;
      }

      /*/rangos
      if (volume.value < 300)
      {
        oxiValveCompMaxTravel = (int)((float)OXI_VALVE_MOTOR_MAX_TRAVEL_A * pow((volSet.value * VOL_STEP_H), OXI_VALVE_EXP_A));
        oxiValveCompMaxTravel = (int)(float)oxiValveCompMaxTravel / (float)((tInsp.value*0.5)+0.5);
        oxiValveCompMaxTravel += OXI_VALVE_MOTOR_MIN_TRAVEL;
        if (oxiValveCompMaxTravel > (OXI_VALVE_MOTOR_MAX_TRAVEL_A * pow(1, OXI_VALVE_EXP_A)) + OXI_VALVE_MOTOR_MIN_TRAVEL)
        {
          oxiValveCompMaxTravel = (int)((float)OXI_VALVE_MOTOR_MAX_TRAVEL_A * pow(1, OXI_VALVE_EXP_A));
          oxiValveCompMaxTravel += OXI_VALVE_MOTOR_MIN_TRAVEL;
        }
      }
      else if (volume.value > 300 && volume.value < 600)
      {
        oxiValveCompMaxTravel = (int)((float)OXI_VALVE_MOTOR_MAX_TRAVEL_B * pow((volSet.value * VOL_STEP_H), OXI_VALVE_EXP_B));
        oxiValveCompMaxTravel = (int)(float)oxiValveCompMaxTravel / (float)((tInsp.value*0.5)+0.5);
        oxiValveCompMaxTravel += OXI_VALVE_MOTOR_MIN_TRAVEL;
        if (oxiValveCompMaxTravel > (OXI_VALVE_MOTOR_MAX_TRAVEL_B * pow(1, OXI_VALVE_EXP_B)) + OXI_VALVE_MOTOR_MIN_TRAVEL)
        {
          oxiValveCompMaxTravel = (int)((float)OXI_VALVE_MOTOR_MAX_TRAVEL_B * pow(1, OXI_VALVE_EXP_B));
          oxiValveCompMaxTravel += OXI_VALVE_MOTOR_MIN_TRAVEL;
        }
      }
      else if (volume.value > 600)
      {
        oxiValveCompMaxTravel = (int)((float)OXI_VALVE_MOTOR_MAX_TRAVEL_C * pow((volSet.value * VOL_STEP_H), OXI_VALVE_EXP_C));
        oxiValveCompMaxTravel = (int)(float)oxiValveCompMaxTravel / (float)((tInsp.value*0.5)+0.5);
        oxiValveCompMaxTravel += OXI_VALVE_MOTOR_MIN_TRAVEL;
        if (oxiValveCompMaxTravel > (OXI_VALVE_MOTOR_MAX_TRAVEL_C * pow(1, OXI_VALVE_EXP_C)) + OXI_VALVE_MOTOR_MIN_TRAVEL)
        {
          oxiValveCompMaxTravel = (int)((float)OXI_VALVE_MOTOR_MAX_TRAVEL_C * pow(1, OXI_VALVE_EXP_C));
          oxiValveCompMaxTravel += OXI_VALVE_MOTOR_MIN_TRAVEL;
        }
      }
      // hasa aca*/
    }

    // Serial.print(openP);
    // Serial.print(" ");

    // Serial.println(stepperOxi.position);

    float openPoxi = 100 * pow(((float)openP * getOxiProp()) / (float)100, VALVE_TO_LINEAL_EXPONET);

    // oxivalve
    microstepToMove = (int)(((float)oxiValveCompMaxTravel / 100.0) * openPoxi) - stepperOxi.position;
    stepperOxi.setRPM(constrain(map(abs(microstepToMove), 0, oxiValveCompMaxTravel, MOTORS_MIN_SPEED, MOTORS_MAX_SPEED), MOTORS_MIN_SPEED, MOTORS_MAX_SPEED));
    stepperOxi.startMove(microstepToMove);
  }
  else if (valve == EXP_VALVE)
  {
    int expValveCompMaxTravel = 0;

    if (!tested)
    {
      (int)((float)EXP_VALVE_MOTOR_MAX_TRAVEL * pow(1, EXP_VALVE_EXP));
      expValveCompMaxTravel += EXP_VALVE_MOTOR_MIN_TRAVEL;
    }
    else
    {
      expValveCompMaxTravel = (int)((float)EXP_VALVE_MOTOR_MAX_TRAVEL * pow((volSet.value * VOL_STEP_H), EXP_VALVE_EXP));
      expValveCompMaxTravel = (int)(float)expValveCompMaxTravel / (float)(((cycleTime - tInsp.value)*0.25)+0.75);
      expValveCompMaxTravel += EXP_VALVE_MOTOR_MIN_TRAVEL;

      /*if (expValveCompMaxTravel > (EXP_VALVE_MOTOR_MAX_TRAVEL * pow(1, EXP_VALVE_EXP)) + EXP_VALVE_MOTOR_MIN_TRAVEL)
      {
        expValveCompMaxTravel = (int)((float)EXP_VALVE_MOTOR_MAX_TRAVEL * pow(1, EXP_VALVE_EXP));
        expValveCompMaxTravel += EXP_VALVE_MOTOR_MIN_TRAVEL;
      }*/

      /*/rangos
      if (volume.value < 300)
      {
        expValveCompMaxTravel = (int)((float)EXP_VALVE_MOTOR_MAX_TRAVEL_A * pow((volSet.value * VOL_STEP_H), EXP_VALVE_EXP_A));
        expValveCompMaxTravel = (int)(float)expValveCompMaxTravel / (float)(((cycleTime - tInsp.value)*0.5)+0.5);
        expValveCompMaxTravel += EXP_VALVE_MOTOR_MIN_TRAVEL;
        if (expValveCompMaxTravel > (EXP_VALVE_MOTOR_MAX_TRAVEL_A * pow(1, EXP_VALVE_EXP_A)) + EXP_VALVE_MOTOR_MIN_TRAVEL)
        {
          expValveCompMaxTravel = (int)((float)EXP_VALVE_MOTOR_MAX_TRAVEL_A * pow(1, EXP_VALVE_EXP_A));
          expValveCompMaxTravel += EXP_VALVE_MOTOR_MIN_TRAVEL;
        }
      }
      else if (volume.value > 300 && volume.value < 600)
      {
        expValveCompMaxTravel = (int)((float)EXP_VALVE_MOTOR_MAX_TRAVEL_B * pow((volSet.value * VOL_STEP_H), EXP_VALVE_EXP_B));
        expValveCompMaxTravel = (int)(float)expValveCompMaxTravel / (float)(((cycleTime - tInsp.value)*0.5)+0.5);
        expValveCompMaxTravel += EXP_VALVE_MOTOR_MIN_TRAVEL;
        if (expValveCompMaxTravel > (EXP_VALVE_MOTOR_MAX_TRAVEL_B * pow(1, EXP_VALVE_EXP_B)) + EXP_VALVE_MOTOR_MIN_TRAVEL)
        {
          expValveCompMaxTravel = (int)((float)EXP_VALVE_MOTOR_MAX_TRAVEL_B * pow(1, EXP_VALVE_EXP_B));
          expValveCompMaxTravel += EXP_VALVE_MOTOR_MIN_TRAVEL;
        }
      }
      else if (volume.value > 600)
      {
        expValveCompMaxTravel = (int)((float)EXP_VALVE_MOTOR_MAX_TRAVEL_C * pow((volSet.value * VOL_STEP_H), EXP_VALVE_EXP_C));
        expValveCompMaxTravel = (int)(float)expValveCompMaxTravel / (float)(((cycleTime - tInsp.value)*0.5)+0.5);
        expValveCompMaxTravel += EXP_VALVE_MOTOR_MIN_TRAVEL;
        if (expValveCompMaxTravel > (EXP_VALVE_MOTOR_MAX_TRAVEL_C * pow(1, EXP_VALVE_EXP_C)) + EXP_VALVE_MOTOR_MIN_TRAVEL)
        {
          expValveCompMaxTravel = (int)((float)EXP_VALVE_MOTOR_MAX_TRAVEL_C * pow(1, EXP_VALVE_EXP_C));
          expValveCompMaxTravel += EXP_VALVE_MOTOR_MIN_TRAVEL;
        }
      }
      // hasa aca*/
    }

    float openPexp = 100 * pow((float)openP  / (float)100, VALVE_TO_LINEAL_EXPONET);

    microstepToMove = (int)(((float)expValveCompMaxTravel / 100.0) * openPexp) - stepperExp.position;
    stepperExp.setRPM(constrain(map(abs(microstepToMove), 0, expValveCompMaxTravel, MOTORS_MIN_SPEED, MOTORS_MAX_SPEED), MOTORS_MIN_SPEED, MOTORS_MAX_SPEED));
    stepperExp.startMove(microstepToMove);
  }
}

void readPresure()
{
  float v = analogRead(PRESURE_PIN) * RESOL_V; // 1.073170732;
  v = presureFilter.updateEstimate(v);
  // presure.value = (v - 201.2) * (10.1972 / 94.8666); //87.0161067
  presure.value = (v - 191) * 0.12;
  presure.value = pow(presure.value, 1); //
  // Serial.print(pSens);
  // Serial.print(" | ");
  // Serial.print(v);
  // Serial.println(" | ");
  // graphUpdate = true;
}

void readSpeed()
{
  float v = analogRead(SPEED_PIN) * RESOL_V;
  v = speedPresureFilter.updateEstimate(v);
  /*
  q.value = ((v - 227.8) / (3.8918247)); //calibracion ciclando
  if (q.value < 0)
    q.value = 0;
  q.value = pow(q.value, 0.5375) * 24.9;
*/
  q.value = ((v - 230) / (3.8918247)); //calibracion ciclando
  if (q.value < 0)
    q.value = 0;
  q.value = pow(q.value, 0.5375) * 25;
  // Serial.print(q.value);
  // Serial.print(",");
}

void calcVolume()
{
  if (!tested) //I made this if twice, so now it stay, but is clever to dont use the tested flag and reset systemC to 0 before each test. But................................. for real the best choice is made the whole program from scratch and be paid for it. :)
  {
    if (cycleState == INSP_OPEN || cycleState == INSP_HOLD || cycleState == INSP_CLOSE)
    { //|| cycleState == INSP_CLOSE)
      volumeIntegration += (q.value * 0.01);
      volume.value = volumeIntegration;
    }
  }
  else
  {
    if (cycleState == INSP_OPEN || cycleState == INSP_HOLD ||  cycleState == INSP_CLOSE) //|| cycleState == INSP_CLOSE)
      volumeIntegration += (q.value * 0.01);

    volume.value = pow(volumeIntegration * 1, 1.0); // - (systemC * pip.value); // compensar el volumen con el valor en de c del systema. K
// 406 marco 458
//
    float k = 1; // constante de multiplicacion
    float e = 1; // exponente
    int vRange = floor(volSet.value / 100);
    switch (vRange)
    {
    case 0: // 0 a 100
      k = 1.43;//1.1;
      e = 1;
      break;
    case 1: // 100 a 200
      k = 1;
      e = 1;
      break;
    case 2: // 200 a 300
      k = 1.105;//0.85;
      e = 1;
      break;
    case 3: // 300 a 400
      k = 1;
      e = 1;
      break;
    case 4: // 400 a 500
      k = 1.04;//0.8;
      e = 1;
      break;
    case 5: // 500 a 600
      k = 1;
      e = 1;
      break;
    case 6: // 600 a 700
      k = 1.02;
      e = 1;
      break;
    case 7: // 700 a 800
      k = 1.05;
      e = 1;
      break;
    case 8: // 800 a 900
      k = 1.05;
      e = 1;
      break;
    case 9: // 900 a 1000
      k = 1;
      e = 1;
      break;
    case 10: // 1000 a 1100
      k = 1;
      e = 1;
      break;
    }

    volume.value = pow(volume.value * k, e);

    if (volume.value < 0)
      volume.value = 0;
  }
}

float getVolSeg()
{
  float p = 1; // exponente
  if (graphMode.value == 0)
  {
    int vRange = floor(volSet.value / 100);
    switch (vRange)
    {
    case 0: // 0 a 100
      p = 0.65;
      break;
    case 1: // 100 a 200
      p = 0.8;
      break;
    case 2: // 200 a 300
      p = 0.9;
      break;
    case 3: // 300 a 400
      p = 0.92;
      break;
    case 4: // 400 a 500
      p = 0.93;
      break;
    case 5: // 500 a 600
      p = 0.95;
      break;
    case 6: // 600 a 700
      p = 0.96;
      break;
    case 7: // 700 a 800
      p = 0.98;
      break;
    case 8: // 800 a 900
      p = 1;
      break;
    case 9: // 900 a 1000
      p = 1;
      break;
    case 10: // 1000 a 1100
      p = 0.99;
      break;
    }
  }
  return p;
}

void doCycle()
{
  // Serial.print(cycleElapsedTime);
  // Serial.print(" | ");

  readPresure();
  readSpeed();

  switch (cycleState)
  {
  case STOP:
    if (run)
      cycleState = START;

    break;

  case START:
    if (cycleCount > 1)
    {
      cycleCount = 0;
      graphBuffDrawCursor = GRAPH_SCALE_TEXT_OFFSET;
    }
    cycleCount++;
    cycleElapsedTime = 0;

    volume.value = 0;
    volumeIntegration = 0;

    presPID.SetTunings(inspKpL, inspKiL, inspKdL);
    presPID.SetMode(AUTOMATIC);

    if (graphMode.value == 0)
    {
      Input = Setpoint = presure.value;
    }
    else
    {
      Input = Setpoint = volume.value;
    }

    presPID.Compute();

    pip.value = remanentPres = presure.value;

    cycleState = INSP_OPEN;
    if(oxig.value != 16)
      digitalWrite(INSP_AIR_VALVE_PIN, HIGH);
    if(oxig.value > 0 )
      digitalWrite(INSP_OXI_VALVE_PIN, HIGH);

    break;

  case INSP_OPEN:
// float valveTime = inspTime - VALVE_CLOSE_TIME - (inspTime * HOLD_PROP);

    // unsigned long valveTime = inspTime - VALVE_CLOSE_TIME;

    if (graphMode.value == 0)
    {
      // modo control por presion 
      Input = presure.value;
      Setpoint = floatMap(pow((cycleElapsedTime / (inspTime - VALVE_CLOSE_TIME - (inspTime * HOLD_PROP))), 0.2), 0, 1, remanentPres, pSet.value * 1);
      presPID.Compute();
      inspValveState = constrain(floatMap(Output, 0, pSet.value, 0, 100), 0, 100);
    }
    else
    {
      // modo de control por volumen
      Input = volume.value;
      Setpoint = floatMap(pow((cycleElapsedTime / (inspTime - VALVE_CLOSE_TIME - (inspTime * HOLD_PROP))), 0.35), 0, 1, 0, volSet.value * 1.0);
      presPID.Compute();
      // inspValveState = constrain(floatMap(Output, 0, Setpoint, 0, 100), 0, 100);
      inspValveState = constrain(floatMap(Output, 0, volSet.value, 0, 100), 0, 100);
    }

    setValveState(INSP_VALVE, inspValveState);

    if (pip.value < presure.value)
      pip.value = presure.value;

    if (cycleElapsedTime > (inspTime - VALVE_CLOSE_TIME - (inspTime * HOLD_PROP)))
    //  ||
    //     ((volume.value > (volSet.value * getVolSeg())) && (graphMode.value == 0)) ||
    //     ((presure.value > pSet.value) && (graphMode.value == 1))
    //   )
    {
      // presPID.SetMode(0);
      Setpoint = Input = presure.value;
      // Output = 0;
      // presPID.Compute();
      // digitalWrite(INSP_AIR_VALVE_PIN, LOW);
      // digitalWrite(INSP_OXI_VALVE_PIN, LOW);
      // cycleState = INSP_CLOSE;
      cycleState = INSP_HOLD;
    }
    break;

  //insp hold
  case INSP_HOLD:

    // modo control por presion 
    Input = presure.value;
    //Setpoint = pSet.value;
    presPID.Compute();
    inspValveState = constrain(floatMap(Output, 0, pSet.value, 0, 100), 0, 100);
  
    setValveState(INSP_VALVE, inspValveState);

    if (pip.value < presure.value)
      pip.value = presure.value;

    if (cycleElapsedTime > inspTime - VALVE_CLOSE_TIME)
    //  ||
    //     ((volume.value > (volSet.value * getVolSeg())) && (graphMode.value == 0)) ||
    //     ((presure.value > pSet.value) && (graphMode.value == 1))
    //   )
    {
      presPID.SetMode(0);
      Setpoint = Input = presure.value;
      Output = 0;
      presPID.Compute();
      digitalWrite(INSP_AIR_VALVE_PIN, LOW);
      digitalWrite(INSP_OXI_VALVE_PIN, LOW);
      cycleState = INSP_CLOSE;
    }
    break;

  case INSP_CLOSE:
    if (inspValveState != 0)
    {
      inspValveState = 0;
      setValveState(INSP_VALVE, inspValveState, 0.1);
    }

    if (pip.value < presure.value)
      pip.value = presure.value;

    if (cycleElapsedTime > inspTime + pInsp.value && stepperInsp.getStepsRemaining() <= 0)
    {

      presPID.SetTunings(expKpL, expKiL, expKdL);

      presPID.SetMode(AUTOMATIC);
      Setpoint = Input = presure.value;
      presPID.Compute();

      remanentPres = presure.value;
      cycleState = EXP_OPEN;
    }
    break;

  case EXP_OPEN:
    // digitalWrite(INSP_VALVE_PIN, LOW);

    if (cycleElapsedTime < inspTime + expTime - VALVE_CLOSE_TIME)
    {
      Input = presure.value;
      Setpoint = floatMap(pow(((cycleElapsedTime - inspTime) / (expTime - 0.45)), 0.1), 0, 1, remanentPres, (peep.value + 0.5));
      presPID.Compute();

      expValveState = constrain(floatMap(Output, 0, -1 * pSet.value, 0, 100), 0, 100);

      setValveState(EXP_VALVE, expValveState);

      realPeep.value = presure.value;
    }
    else
    {
      presPID.SetMode(0);
      cycleState = EXP_CLOSE;
    }
    break;

  case ALARM_INSP_CLOSE:
    presPID.SetMode(0);
    setValveState(INSP_VALVE, 0);
    setValveState(EXP_VALVE, 100);
    if (cycleElapsedTime > inspTime + expTime - VALVE_CLOSE_TIME)
      cycleState = EXP_CLOSE;
    break;

  case EXP_CLOSE:
    if (expValveState != 0)
    {
      expValveState = 0;
      setValveState(EXP_VALVE, expValveState, 0.1);
      Setpoint = Input = presure.value;
      Output = 0;
      presPID.Compute();
    }

    if (cycleElapsedTime > inspTime + expTime && stepperExp.getStepsRemaining() <= 0)
    {
      if (run)
      {
        cycleState = START;
      }
      else
      {
        cycleState = STOP;
      }
    }
    break;
  }

  if (run)
  {

    //Serial.print(cycleState);
    //Serial.print(",");
    Serial.print(presure.value);
    Serial.print(",");
    //Serial.print(volume.value);
    //Serial.print(",");
    //Serial.print(q.value);
    //Serial.print(",");
    Serial.print(Setpoint);
    Serial.print(",");
    // Serial.print(Output);
    // Serial.print(",");
    Serial.print(inspValveState);
    Serial.print(",");
    Serial.print(expValveState);
    Serial.println();
  }
}

void constantFlow()
{
  digitalWrite(INSP_AIR_VALVE_PIN, HIGH);
  digitalWrite(INSP_OXI_VALVE_PIN, HIGH);

  bool x = false; // si es X
  float a = 0;

  while (true)
  {
    readPresure();
    readSpeed();
    // Input = presure.value;
    // Setpoint = pSet.value;
    // presPID.Compute();

    if (!digitalRead(ENC_BTN))
    {
      x = !x;
      while (!digitalRead(ENC_BTN))
      {
      }
    }

    if (x)
    {
      a += encVal;
    }
    else
    {
      oxig.value += encVal;
    }
    encVal = 0;

    // inspValveState += (float)encVal * 0.1; //constrain(floatMap(Output, 0, pSet.value, 0, 100), 0, 100);
    a = constrain(a, 0, 100);
    oxig.value = constrain(oxig.value, 0, 8);

    setValveState(INSP_VALVE, a);

    u8g2.clearBuffer();
    u8g2.setCursor(10, 20);
    if (!x)
      u8g2.print(">");
    u8g2.print("Oxig % = ");
    u8g2.print(oxSteps[(int)oxig.value]);
    u8g2.setCursor(10, 30);
    if (x)
      u8g2.print(">");
    u8g2.print("Apertura % = ");
    u8g2.print(a);
    u8g2.setCursor(10, 40);
    u8g2.print("P = ");
    u8g2.print(presure.value);
    u8g2.setCursor(10, 50);
    u8g2.print("Q = ");
    u8g2.print(q.value);
    u8g2.sendBuffer();
  }
}

void clearAlarms()
{
  pendingNotif = false;
  alarmTimer = 0;
  alarm = false;
  drawNotif(CLEAR_NTIF);
  if (fullScreenAlarm)
  {
    fullScreenAlarm = false;
    showHome();
  }
}

void doAlarms()
{
  byte currentAlarm = NO_ALARM;

  if (presure.value > pAlMax.value && !presureMaxAlarm)
  {
    cycleState = ALARM_INSP_CLOSE;
    currentAlarm = P_MAX;
    alarmTimer = (ALARM_TIME * 10) + 1;
    presureMaxAlarm = true;
  }
  if (presure.value < pAlMax.value)
  {
    presureMaxAlarm = false;
  }

  if (presure.value < pAlMin.value && !presureMinAlarm)
  {
    // presureMinAlarm = true;
    currentAlarm = P_MIN;
  }
  // if (presure.value > pAlMin.value)
  // {
  //   presureMinAlarm = false;
  // }

  // if (presure.value > pSet.value && !presureNotif)
  // {
  //   presureNotif = true;
  //   drawNotif(P_NTIF);
  // }
  // if (presure.value < pSet.value)
  // {
  //   presureNotif = false;
  // }

  if (cycleState == INSP_CLOSE && volume.value < volAlMin.value)
  {
    cycleState = ALARM_INSP_CLOSE;
    currentAlarm = V_MIN;
    alarmTimer = (ALARM_TIME * 10) + 1;
    volumeMaxAlarm = true;
  }
  // if (volume.value < volAlMin.value)
  // {
  //   volumeMaxAlarm = false;
  // }

  if (volume.value > volAlMax.value && !volumeMaxAlarm)
  {
    cycleState = ALARM_INSP_CLOSE;
    currentAlarm = V_MAX;
    alarmTimer = (ALARM_TIME * 10) + 1;
    volumeMaxAlarm = true;
  }
  if (volume.value < volAlMax.value)
  {
    volumeMaxAlarm = false;
  }

  if (currentAlarm == NO_ALARM)
    alarmTimer = 0;

  if (alarmTimer > ALARM_TIME * 10 && currentAlarm != NO_ALARM)
  {
    alarm = true;
    drawNotif(currentAlarm, true);
  }
}

void doButton()
{

  // RUN / STOP - RED BUTTON
  if (!digitalRead(RED_BUTTON) && !redButtonState)
  {
    redButtonState = true;
    run = !run;
  }
  if (digitalRead(RED_BUTTON))
    redButtonState = false;

  // ENCODER BUTTON
  if (!digitalRead(ENC_BTN) && !encBtnState)
  {
    buttonChange = true;
    encBtnState = true;

    // Clear alarms or notif is exist
    if (pendingNotif)
    {
      clearAlarms();
    }
    else
    {
      if (setInfoMenu)
      {
        if (setNavMenu)
        {
          // if is on setting page toggle to edit parameter.
          setNavSetting = !setNavSetting;
        }
        else
        {
          // if not show setting page back button
          if (paramsMenu[setIndexMenu]->id == back.id)
          {
            storeAll();
            showHome();
          }
          else
          {
            showSettingPage();
          }
        }
      }
      else
      {
        showSettingsMenu();
      }
    }
  }

  if (digitalRead(ENC_BTN))
    encBtnState = false;
}

void initMotors()
{

  stepperInsp.begin(RPM, MICROSTEPS);
  stepperOxi.begin(RPM, MICROSTEPS);
  stepperExp.begin(RPM, MICROSTEPS);

  stepperInsp.setEnableActiveState(LOW);
  stepperOxi.setEnableActiveState(LOW);
  stepperExp.setEnableActiveState(LOW);

  stepperInsp.enable();
  stepperOxi.enable();
  stepperExp.enable();

  //x inspiration air valve
  stepperInsp.setRPM(10);
  stepperInsp.startMove(-20000);
  while (digitalRead(X_MIN_PIN) && stepperInsp.nextAction() != 0)
  {
    delayMicroseconds(33);
  }
  stepperInsp.stop();
  stepperInsp.setRPM(200);
  stepperInsp.startMove(INSP_VALVE_MOTOR_STEPS_FROM_HOME);
  while (stepperInsp.nextAction() != 0)
  {
    delayMicroseconds(33);
  }
  stepperInsp.stop();

  //y inspiration oxigen valve
  stepperOxi.setRPM(10);
  stepperOxi.startMove(-20000);
  while (digitalRead(Y_MIN_PIN) && stepperOxi.nextAction() != 0)
  {
    delayMicroseconds(33);
  }
  stepperOxi.stop();
  stepperOxi.setRPM(200);
  stepperOxi.startMove(OXI_VALVE_MOTOR_STEPS_FROM_HOME);
  while (stepperOxi.nextAction() != 0)
  {
    delayMicroseconds(33);
  }
  stepperOxi.stop();

  // z expiration valve
  stepperExp.setRPM(10);
  stepperExp.startMove(-20000);
  while (digitalRead(Z_MIN_PIN) && stepperExp.nextAction() != 0)
  {
    delayMicroseconds(33);
  }
  stepperExp.stop();
  stepperExp.setRPM(200);
  stepperExp.startMove(EXP_VALVE_MOTOR_STEPS_FROM_HOME);
  while (stepperExp.nextAction() != 0)
  {
    delayMicroseconds(33);
  }
  stepperExp.stop();

  stepperOxi.position = stepperInsp.position = stepperExp.position = 0;
}

bool initializationTest()
{
  pSet.value = TEST_PRESURE;
  tInsp.value = TEST_TINSP;
  freq.value = 30;
  graphMode.value = 0;

  setCycle();

  if (!digitalRead(RED_BUTTON))
    constantFlow();
  bool pResult = false;
  bool cResult = false;
  float c = 0;

  while (!(pResult && cResult))
  {
    drawMessage(F("Tape la salida del|circuito corrugado y|presione la perilla para|continuar."));

    while (digitalRead(ENC_BTN))
    {
      //wait for btn press
    }
    drawMessage(F("Testeando presion ..."));

    //do insp
    cycleState = START;
    while (cycleState != EXP_OPEN)
    {
      doCycle();
    }

    float pa = presure.value;
    u8g2.setCursor(10, 30);
    u8g2.print("Pa = ");
    u8g2.print(pa);
    u8g2.sendBuffer();

    //wait 3 sec
    int i = TEST_INTERVAL * 1000;
    while (i > 0)
    {
      readPresure();
      delay(1);
      i--;
    }

    float pb = presure.value;

    //see presure loss
    float xx = ((pb - pa) / TEST_INTERVAL) * TEST_INTERVAL;

    u8g2.setCursor(10, 40);
    u8g2.print("Pb = ");
    u8g2.print(pb);
    u8g2.setCursor(10, 50);
    u8g2.print("xx = ");
    u8g2.print(xx);
    u8g2.sendBuffer();

    //trminar el ciclo que se paro en exp_open
    while (cycleState != STOP)
    {
      doCycle();
    }

    if (pa < 10)
    {
      drawMessage(F("Error en test de presion min!|Tubo corrugado defectuoso|o desconectado.|----|Pulse la perilla|para continuar."));
      while (digitalRead(ENC_BTN))
      {
        //wait for btn press
      }
      while (!digitalRead(ENC_BTN))
      {
        //wait for btn press
      }
    }

    //fin test de presion
    delay(4000);

    if (xx > DELTA_P)
    {
      //do insp
      pResult = true;
      drawMessage(F("Testeando C ..."));
      c = 0;
      cycleState = START;
      while (cycleState != STOP)
      {
        doCycle();
        if (presure.value > C_CALC_PRESURE && c == 0)
        {
          c = volume.value / presure.value; //calulate compilance
          u8g2.setCursor(10, 30);
          u8g2.print("V ");
          u8g2.print(volume.value);
          u8g2.print(" / P ");
          u8g2.print(presure.value);
          u8g2.setCursor(10, 40);
          u8g2.print("C = ");
          u8g2.println(c);
          u8g2.sendBuffer();
        }
      }
      //fin test de C
      delay(4000);
    }
    else
    {
      drawMessage(F("Error en test de presion!|Tubo corrugado defectuoso|o desconectado.|----|Pulse la perilla|para continuar."));
      while (digitalRead(ENC_BTN))
      {
        //wait for btn press
      }
      while (!digitalRead(ENC_BTN))
      {
        //wait for btn press
      }
    }

    if (pResult)
    {
      if (c < SYSTEM_C && (c != 0))
      {
        drawMessage(F("Calibracion exitosa.|----|Pulse la perilla para continuar."));
        cResult = true;
        while (digitalRead(ENC_BTN))
        {
          //wait for btn press
        }
        while (!digitalRead(ENC_BTN))
        {
          //wait for btn press
        }
      }
      else
      {
        drawMessage(F("Error en test de C!|Tubo corrugado defectuoso|o desconectado.|----|Pulse la perilla|para continuar."));
        while (digitalRead(ENC_BTN))
        {
          //wait for btn press
        }
        while (!digitalRead(ENC_BTN))
        {
          //wait for btn press
        }
      }
    }
  }
  //reset info values.
  volume.value = 0;
  volumeIntegration = 0;
  presure.value = 0;
  pip.value = 0;
  ie.value = 0;
  q.value = 0;

  systemC = c;
  tested = true;

  return true;
}

// ----------------------------------  SETUP
void setup(void)
{

  Serial.begin(230400);
  // Serial.print("Start....");

  pinMode(RED_BUTTON, INPUT_PULLUP);

  pinMode(ENC_BTN, INPUT_PULLUP);

  analogReference(ANALOG_REF);

  r.begin();

  pinMode(BUZZ_PIN, OUTPUT);
  pinMode(INSP_AIR_VALVE_PIN, OUTPUT);
  pinMode(INSP_OXI_VALVE_PIN, OUTPUT);

  digitalWrite(INSP_AIR_VALVE_PIN, LOW);
  digitalWrite(INSP_OXI_VALVE_PIN, LOW);

  //endstop
  pinMode(X_MIN_PIN, INPUT_PULLUP);
  pinMode(Y_MIN_PIN, INPUT_PULLUP);
  pinMode(Z_MIN_PIN, INPUT_PULLUP);

  initMotors();

  // recall parameters
  if (recallStoredState() == EPPROM_V)
    recallAll();

  //display init
  u8g2.begin();
  u8g2.setFont(u8g2_font_5x7_mf);
  u8g2.clearBuffer();

  presPID.SetSampleTime(SampleTime);
  presPID.SetMode(AUTOMATIC);

  cli(); //stop interrupts

  //set timer2 interrupt for motor control at 1khz
  TCCR2A = 0; // set entire TCCR2A register to 0
  TCCR2B = 0; // same for TCCR2B
  TCNT2 = 0;  //initialize counter value to 0
  // set compare match register for 8khz increments
  OCR2A = 250; // = (16*10^6) / (8000*8) - 1 (must be <256) //60 es 30 micros 20 es 10
  // turn on CTC mode
  TCCR2A |= (1 << WGM21);
  // Set CS bits for 64 prescaler
  TCCR2B |= (1 << CS22);
  //32 prescaler
  // TCCR2B |= (1 << CS21) | (1 << CS20);
  // enable timer compare interrupt
  TIMSK2 |= (1 << OCIE2A);

  //set timer1 for periodic updates to 1000Hz
  TCCR1A = 0; // set entire TCCR1A register to 0
  TCCR1B = 0; // same for TCCR1B
  TCNT1 = 0;  //initialize counter value to 0
  // set compare match register for 1hz increments
  OCR1A = 2; // = (16*10^6) / (1*1024) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12 bits for 256 prescaler
  TCCR1B |= (1 << CS12); //| (1 << CS10);
  // // Set CS11 bits for 8 prescaler
  // TCCR1B |= (1 << CS11); //| (1 << CS10);
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  sei();

  initializationTest();

  // recall parameters
  if (recallStoredState() == EPPROM_V)
    recallAll();

  showHome();
}

// ----------------------------------  LOOP
void loop(void)
{
  // Serial.print(millis() - startTime);
  // Serial.print(",");
  // startTime = millis();

  if (run)
  {
    doAlarms();
  }
  else
  {
    alarmTimer = 0;
  }

  doButton();

  doSetting();

  if (recalcCycle)
  {
    recalcCycle = false;
    setCycle();
  }

  doCycle();

  if (fullScreenAlarm)
  {
  }
  else
  {
    if (!setInfoMenu)
    {
      if (run)
      {
        if (pauseShowed)
        {
          pauseShowed = false;
          showHome();
        }

        drawInfo();

        if (graphUpdate)
        {
          drawGraph();
          graphUpdate = false;
        }
      }
      else if (!pauseShowed)
      {
        showHome();
      }
    }
    else
    {
      if (!setNavMenu)
      {
        drawSttingsMenu();
      }
      else
      {
        drawSettingPage();
      }
    }
    updateScreen();
  }

  // Serial.println();
}

// ----------------------------------  ISR
ISR(TIMER2_COMPA_vect) // timmed actions cicle time, graph update, encoder.
{
  //each 100 millis rutine
  beepMillis++;
  millisCounter += 1;
  millisCounterGraph += 1;

  cycleElapsedTime += 0.001;
  if (millisCounter > 10)
  {
    millisCounter = 0;

    calcVolume();
  }

  if (beepMillis > 100)
  {
    beepMillis = 0;
    infoUpdate = true;
    alarmTimer++;

    if (alarm || sound)
    {
      sound = !sound;
      digitalWrite(BUZZ_PIN, sound);
    }
  }

  if (millisCounterGraph > (float)(((cycleTime * 2) + pInsp.value) / 118) / 0.001)
  {
    millisCounterGraph = 0;
    graphUpdate = true;
  }

  //encoder read
  unsigned char result = r.process();
  if (result == DIR_CW)
  {
    encVal++;
    // Serial.println(encVal);
  }
  else if (result == DIR_CCW)
  {
    encVal--;
    // Serial.println(encVal);
  }
}

ISR(TIMER1_COMPA_vect) // motor step interrupt.
{
  TIMSK2 |= (0 << OCIE1A);

  stepperInsp.nextAction();
  stepperOxi.nextAction();
  stepperExp.nextAction();

  TIMSK2 |= (1 << OCIE1A);
}